use std::collections::VecDeque;
use std::time::Instant;


fn main() {
    println!("Hello, world!");
    let input = include_str!("./input.txt");

    let start = Instant::now();
    part1(input);
    println!("part1 took : {:?}", start.elapsed());
    let start = Instant::now();
    part2(input);
    println!("part2 took : {:?}", start.elapsed());
}

fn part1(input: &str) {
    let mut monkeys = extract_data(input);
    for _ in 0..20 {
        for i in 0..monkeys.len() {
          //  let monkey = &mut monkeys[i];
            while monkeys[i].items.len() > 0 {
                let mut item = monkeys[i].items.pop_front().unwrap();
                let operation_value = if monkeys[i].operation.1 == "old" {
                    item
                } else {
                    monkeys[i].operation.1.parse::<i64>().unwrap()
                };
                let a = monkeys[i].operation.0.as_str();
                match a {
                    "*" => item *= operation_value,
                    "+" => item += operation_value,
                    _ => {}
                }
                item /= 3;


                if item % monkeys[i].divide_by == 0 {
                    let indice = monkeys[i].next_monkeys.0;
                    monkeys[indice].items.push_back(item);
                } else {
                    let indice = monkeys[i].next_monkeys.1;
                    monkeys[indice].items.push_back(item);
                }
                monkeys[i].tossed_items += 1;
            }
        }
    }

    monkeys.sort_by(|a, b| b.tossed_items.cmp(&a.tossed_items));
    println!("total is {}", monkeys.iter().take(2).fold(1, |acc, monkey| acc * monkey.tossed_items));
}

fn part2(input: &str) {
    let mut monkeys = extract_data(input);
    let modulo = monkeys.iter().fold(1, |acc, monkey| acc * monkey.divide_by);
    for _ in 0..10000 {
        for i in 0..monkeys.len() {
            //  let monkey = &mut monkeys[i];
            while monkeys[i].items.len() > 0 {
                let mut item = monkeys[i].items.pop_front().unwrap();
                let operation_value = if monkeys[i].operation.1 == "old" {
                    item
                } else {
                    monkeys[i].operation.1.parse::<i64>().unwrap()
                };
                let a = monkeys[i].operation.0.as_str();
                match a {
                    "*" => item *= operation_value,
                    "+" => item += operation_value,
                    _ => {}
                }
                item = item % modulo;

                if item % monkeys[i].divide_by == 0 {
                    let indice = monkeys[i].next_monkeys.0;
                    monkeys[indice].items.push_back(item);
                } else {
                    let indice = monkeys[i].next_monkeys.1;
                    monkeys[indice].items.push_back(item);
                }
                monkeys[i].tossed_items += 1;
            }
        }
    }

    monkeys.sort_by(|a, b| b.tossed_items.cmp(&a.tossed_items));
    println!("total is {}", monkeys.iter().take(2).fold(1, |acc, monkey| acc * monkey.tossed_items));
}

#[derive(Debug)]
struct Monkey {
    items: VecDeque<i64>,
    operation: (String, String),
    divide_by: i64,
    next_monkeys: (usize, usize),
    tossed_items: i64
}

impl Monkey {
    pub fn new() -> Self {
        Self {
            items: VecDeque::new(),
            operation: (String::new(), String::new()),
            divide_by: 1,
            next_monkeys: (0, 0),
            tossed_items: 0
        }
    }
}

fn extract_data(input: &str) -> Vec<Monkey> {
    input
        .split("\r\n\r\n")
        .map(|group| {
            let mut monkey = Monkey::new();
            for line in group.split("\r\n") {
                if line.starts_with("Monkey") {
                    continue;
                }
                if line.starts_with("  S") {
                    line.split_once("items: ")
                        .unwrap()
                        .1
                        .split(", ")
                        .map(|item| item.parse::<i64>().unwrap())
                        .for_each(|item_val| monkey.items.push_back(item_val));
                    continue;
                }
                if line.starts_with("  O") {
                    let (ope, val) = line.split_once("old ").unwrap().1.split_once(" ").unwrap();
                    monkey.operation = (ope.to_string(), val.to_string());
                    continue;
                }
                if line.starts_with("  T") {
                    monkey.divide_by = line.split_once(" by ").unwrap().1.parse::<i64>().unwrap();
                    continue;
                }
                if line.starts_with("    If true:") {
                    monkey.next_monkeys.0 = line
                        .split_once("to monkey ")
                        .unwrap()
                        .1
                        .parse::<usize>()
                        .unwrap();
                }
                if line.starts_with("    If false:") {
                    monkey.next_monkeys.1 = line
                        .split_once("to monkey ")
                        .unwrap()
                        .1
                        .parse::<usize>()
                        .unwrap();
                }
            }
            monkey
        })
        .collect::<Vec<Monkey>>()
}
