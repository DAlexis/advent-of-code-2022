use std::collections::HashMap;

fn main() {
    println!("Hello, world!");
    let input = include_str!("input.txt");

    part1(input);

    part2(input);

}

fn part1(input : &str) -> Vec<Node> {
    let mut current = 0;
    let mut name_to_index_map : HashMap<String, u16> = HashMap::new();
    let mut tree : Vec<Node> = Vec::new();
    for line in input.lines() {
        let (cmd, arg) = line.split_once(" ").unwrap();
        match cmd {
            "$" => {

                if arg.starts_with("ls") {

                } else if arg.starts_with("cd") {
                    create_node(&mut current, &mut tree, arg);
                }
            },
            "dir" => {

            }
            _ => {
                //file with size
                let size = cmd.parse::<u32>().unwrap();
                add_size(current, &mut tree, size);
            }
        }
    }
    let sum = tree.iter().filter(|node| node.size <= 100000).map(|node| node.size).sum::<u32>();
    println!("result is : {:?}", sum);
    tree
}

fn part2(input : &str) {
    let mut tree : Vec<Node> = part1(input);

    let free = 70000000 - tree.get(0).unwrap().size;
    println!("free is : {:?}", free);
    let needed =  30000000 - free;

    println!("needed is : {:?}", needed);

    let sum = tree.iter().filter(|node| node.size >= needed).map(|node| node.size).min().unwrap();
    println!("result is : {:?}", sum);
}

fn create_node(current: &mut usize, tree: &mut Vec<Node>, arg: &str) {
    let (cmd, name) = arg.split_once(" ").unwrap();
    if name == ".." {
        *current = if let Some(current_node) = tree.get(*current).unwrap().parent {
            current_node
        } else {
            0
        };
        return;
    }
    let parent = if name == "/" { None } else { Some(*current) };
    let node = Node {
        name: name.to_string(),
        size: 0,
        parent,
        children: vec![]
    };
    tree.push(node);
    *current = tree.len() - 1;
    if let Some(parent_index) = parent {
        if let Some(parent_node) = tree.get_mut(parent_index as usize) {
            parent_node.children.push(*current)
        }
    }
}

fn add_size(current: usize, tree: &mut Vec<Node>, size: u32) {

    let node_current = tree.get_mut(current).unwrap();
    node_current.size = node_current.size + size;
    if let Some(parent_index) = node_current.parent {
        add_size(parent_index, tree, size);
    }
}

#[derive(Debug)]
struct Node {
    name: String,
    size: u32,
    parent: Option<usize>,
    children: Vec<usize>
}