fn main() {
    println!("Hello, world!");
    let input = include_str!("input.txt");

    part2(input);
}

fn part2(input: &str) {
    let mut current: (isize, isize) = (0, 0);
    let mut vec_all = vec![vec![false; 1000]; 300];
    let mut lowest_rock: isize = 0;
    input.lines().for_each(|line| {
        line.split(" -> ").for_each(|coord| {
            let (width, height) = coord.split_once(",").unwrap();
            let to = (
                height.parse::<isize>().unwrap(),
                width.parse::<isize>().unwrap(),
            );
            if to.0 > lowest_rock {
                lowest_rock = to.0;
            }
            if current == (0, 0) {
                current = to;
            } else {
                fill(current, to, &mut vec_all);
                current = to;
            }
        });
        current = (0, 0);
    });

    for i in 0..1000 {
        vec_all[(lowest_rock + 2) as usize][i] = true;
    }

    let mut total = 0;
    let mut done = false;
    while !done {
        let mut current = (0, 500);
        while tick_has_moved(&mut vec_all, &mut current) {

        }
        if !done {
            total += 1;
        }
        if current.0 == 0 {
            done = true;
        }


    }

    println!("{}", total);
}

fn tick_has_moved(vec_all: &mut Vec<Vec<bool>>, current: &mut (usize, usize)) -> bool {
    if !vec_all[current.0 + 1][current.1] {
        current.0 += 1;
        return true;
    }
    if !vec_all[current.0 + 1][current.1 - 1] {
        current.0 += 1;
        current.1 -= 1;
        return true;
    }
    if !vec_all[current.0 + 1][current.1 + 1] {
        current.0 += 1;
        current.1 += 1;
        return true;
    }
    vec_all[current.0][current.1] = true;
    return false;
}

fn fill(from: (isize, isize), to: (isize, isize), vec: &mut Vec<Vec<bool>>) {
    let mut dir: (isize, isize) = (0, 0);

    if from.0 < to.0 {
        dir = (1, 0);
    } else if from.0 > to.0 {
        dir = (-1, 0);
    } else if from.1 < to.1 {
        dir = (0, 1);
    } else if from.1 > to.1 {
        dir = (0, -1);
    }
    let mut current = (from.0, from.1);
    vec[current.0 as usize][current.1 as usize] = true;
    while current.0 != to.0 || current.1 != to.1 {
        current.0 += dir.0;
        current.1 += dir.1;
        vec[current.0 as usize][current.1 as usize] = true;
    }
}
