use std::time::Instant;

fn main() {
    let input = include_str!("input.txt");

    let start = Instant::now();
    day1(input);
    println!("day1 took : {:?}", start.elapsed());

    let start = Instant::now();
    day2(input);
    println!("day2 took : {:?}", start.elapsed());
}

fn day1(input: &str) {
    let sum = input
        .lines()
        .map(|line| {

            return match line {
                "A X" => 4,
                "A Y" => 8,
                "A Z" => 3,
                "B X" => 1,
                "B Y" => 5,
                "B Z" => 9,
                "C X" => 7,
                "C Y" => 2,
                "C Z" => 6,
                _ => 0
            };
        })
        .sum::<i32>();

    println!("{:?}", sum);
}
fn day2(input: &str) {
    let sum = input
        .lines()
        .map(|line| {
            return match line {
                "A X" => 3,
                "A Y" => 4,
                "A Z" => 8,
                "B X" => 1,
                "B Y" => 5,
                "B Z" => 9,
                "C X" => 2,
                "C Y" => 6,
                "C Z" => 7,
                _ => 0
            };
        })
        .sum::<i32>();

    println!("{:?}", sum);
}
