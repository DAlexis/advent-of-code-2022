use std::collections::HashSet;
use std::io::BufRead;
use std::time::Instant;

fn main() {

    let mut lines = include_str!("./input.txt")
        .lines()
        .into_iter()
        .map(|line| {
            line.split("")
                .filter(|chara| chara.len() > 0)
                .map(|num| num.parse::<i32>().unwrap())
                .collect::<Vec<i32>>()
        })
        .collect::<Vec<Vec<i32>>>();

    let start = Instant::now();
    part1(&mut lines);
    println!("day1 took : {:?}", start.elapsed());

    let start = Instant::now();
    part2(&mut lines);
    println!("day2 took : {:?}", start.elapsed());

}

fn part1(lines: &mut Vec<Vec<i32>>) -> HashSet<(usize, usize)> {
    let height  = lines.len();
    let mut sum = height * 2 +  ((height -2) *2);


    let mut visible_tree_set: HashSet<(usize, usize)> = HashSet::new();
    compare_on_lines(lines, height, &mut visible_tree_set);
    compare_on_collumn(lines, height, &mut visible_tree_set);
    sum = sum + visible_tree_set.len();
    println!("{}", sum);
    visible_tree_set
}

fn compare_on_lines(lines: &mut Vec<Vec<i32>>, height: usize, visible_tree_set: &mut HashSet<(usize, usize)>) {
    for i in 1..height - 1 {
        let mut ext_heigth = lines[i][0];
        for j in 1..height - 1 {
            if lines[i][j] > ext_heigth {
                visible_tree_set.insert((i, j));
                ext_heigth = lines[i][j];
            }
        }
        let mut ext_heigth = lines[i][height - 1];
        for j in (1..height - 1).rev() {
            if lines[i][j] > ext_heigth {
                visible_tree_set.insert((i, j));
                ext_heigth = lines[i][j];
            }
        }
    }
}



fn compare_on_collumn(lines: &mut Vec<Vec<i32>>, height: usize, visible_tree_set: &mut HashSet<(usize, usize)>) {
    for j in 1..height - 1 {
        let mut ext_heigth = lines[0][j];
        for i in 1..height - 1 {
            if lines[i][j] > ext_heigth {
                visible_tree_set.insert((i, j));
                ext_heigth = lines[i][j];
            }
        }
        let mut ext_heigth = lines[height - 1][j];
        for i in (1..height - 1).rev() {
            if lines[i][j] > ext_heigth {
                visible_tree_set.insert((i, j));
                ext_heigth = lines[i][j];
            }
        }
    }
}

fn part2(lines: &mut Vec<Vec<i32>>) {
    let visible_tree_set: HashSet<(usize, usize)> = part1(lines);
    let height  = lines.len();
    let max = visible_tree_set.iter().map(|tree| {
        let mut score = 1;
        let dir_score = check_up(lines, tree);
        score = dir_score * score;

        let dir_score = check_down(lines, tree);
        score = dir_score * score;


        let dir_score = check_right(lines, tree);
        score = dir_score * score;

        let dir_score = check_left(lines, tree);
        score = dir_score * score;

     //   println!("tree {:?} {:?}", tree, score);
        score
    }).max().unwrap();
    println!("{:?}", max);
}

fn check_up(lines: &mut Vec<Vec<i32>>, tree: &(usize, usize)) -> i32 {
    let mut height = lines[tree.0][tree.1];
    let mut dir_score = 0;
    for i in (0..tree.0).rev() {
        if lines[i][tree.1] < height {
            dir_score = dir_score + 1;
        } else {
            dir_score = dir_score + 1;
            break;
        }
    }
    dir_score
}

fn check_down(lines: &mut Vec<Vec<i32>>, tree: &(usize, usize)) -> i32 {
    let mut height = lines[tree.0][tree.1];
    let mut dir_score = 0;
    for i in (tree.0+1)..lines.len() {
        if lines[i][tree.1] < height {
            dir_score = dir_score + 1;
        } else {
            dir_score = dir_score + 1;
            break;
        }
    }
    dir_score
}

fn check_right(lines: &mut Vec<Vec<i32>>, tree: &(usize, usize)) -> i32 {
    let mut height = lines[tree.0][tree.1];
    let mut dir_score = 0;
    for j in tree.1+1..lines.len() {
        if lines[tree.0][j] < height {
            dir_score = dir_score + 1;
        } else {
            dir_score = dir_score + 1;
            break;
        }
    }
    dir_score
}

fn check_left(lines: &mut Vec<Vec<i32>>, tree: &(usize, usize)) -> i32 {
    let mut height = lines[tree.0][tree.1];
    let mut dir_score = 0;
    for j in (0..tree.1).rev() {
        if lines[tree.0][j] < height {
            dir_score = dir_score + 1;
        } else {
            dir_score = dir_score + 1;
            break;
        }
    }
    dir_score
}
