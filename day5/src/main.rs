use std::collections::VecDeque;
use std::time::Instant;

fn main() {
    println!("Hello, world!");
    let input = include_str!("input.txt");
    let (stack, moves) = input.split_once("\n\n").unwrap();



    let start = Instant::now();
    day1(stack, moves);
    println!("day1 took : {:?}", start.elapsed());

    let start = Instant::now();
    day2(stack, moves);
    println!("day1 took : {:?}", start.elapsed());
}

fn parse_stack( stack: &str) -> Vec<VecDeque<char>>{
    let mut vec_dequeue_list : Vec<VecDeque<char>> = vec![VecDeque::new();9];

    let stack_per_line = stack.split("\n").collect::<Vec<&str>>();
    stack_per_line.iter().take(stack_per_line.len() - 1)
        .for_each(|line| {
            let chars_list = line.chars().collect::<Vec<char>>();
            let mut index = 0;
            for chunk in chars_list.chunks(4) {
                if chunk[1] != ' ' {
                    vec_dequeue_list[index].push_front(chunk[1]);
                }
                index = index + 1;
            }
        });
    vec_dequeue_list
}

fn parse_moves(moves: &str) -> Vec<(i32, usize, usize)> {
    let mut move_list: Vec<(i32, usize, usize)> = Vec::new();
    moves.lines().for_each(|line| {
        let vec_input = line.split_whitespace().collect::<Vec<&str>>();
        move_list.push((
            vec_input[1].parse::<i32>().unwrap(),
            vec_input[3].parse::<usize>().unwrap(),
            vec_input[5].parse::<usize>().unwrap()
        ))
    });

    move_list
}

fn day1(stack: &str, moves: &str) {
    let mut vecDequeue_array  = parse_stack(stack);
    let move_list = parse_moves(moves);
    move_list.iter().for_each(|move_line| {
        let (number, from, to) = move_line;
        for _ in 0..*number {
            let value = vecDequeue_array[from - 1].pop_back().unwrap();
            vecDequeue_array[to - 1].push_back(value);
        }
    });
    let res = vecDequeue_array.iter_mut().map(|vec| vec.pop_back().unwrap()).collect::<Vec<char>>();
    println!("{:?}", res);
}

fn day2(stack: &str, moves: &str) {
    let mut vecDequeue_array  = parse_stack(stack);
    let move_list = parse_moves(moves);
    move_list.iter().for_each(|move_line| {
        let (number, from, to) = move_line;
        let mut vec_to_add: Vec<char> = Vec::new();
        for _ in 0..*number {
            vec_to_add.push( vecDequeue_array[from - 1].pop_back().unwrap());
        }
        vec_to_add.reverse();
       for to_add in vec_to_add {
            vecDequeue_array[to - 1].push_back(to_add);
        }
    });
    let res = vecDequeue_array.iter_mut().map(|vec| vec.pop_back().unwrap()).collect::<Vec<char>>();
    println!("{:?}", res);
}