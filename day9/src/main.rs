use std::collections::HashSet;
use std::time::Instant;

fn main() {
    println!("Hello, world!");
    let input = include_str!("./input.txt")
        .lines()
        .map(|line| {
            let (dir, num) =line.split_once(" ").unwrap();
            let num = num.parse::<u8>().unwrap();
            (dir, num)
        })
        .collect::<Vec<(&str, u8)>>();
    let start = Instant::now();
    part1(&input);
    println!("day1 took : {:?}", start.elapsed());

    let start = Instant::now();
    part2(&input);
    println!("day1 took : {:?}", start.elapsed());
}

fn part1(input_vec: &Vec<(&str, u8)>) {
    let mut pos_set : HashSet<Pos> = HashSet::new();
    let pos_start  = Pos::new(0,0);
    pos_set.insert(pos_start);
    let (mut head, mut tail) : (Pos, Pos) = (Pos::new(0,0), Pos::new(0,0));
    for (dir, num) in input_vec {
        match dir {
            &"U" => {execute_n_moves((0,1), (&mut head, &mut tail), *num, &mut pos_set)},
            &"R" => {execute_n_moves((1,0), (&mut head, &mut tail), *num, &mut pos_set)},
            &"D" => {execute_n_moves((0,-1), (&mut head, &mut tail), *num, &mut pos_set)},
            &"L" => {execute_n_moves((-1,0), (&mut head, &mut tail), *num, &mut pos_set)},
            _ => {}
        }
    }
    println!("part1 res is {}", pos_set.len());
}
fn execute_n_moves(dir: (i16, i16), (head, tail) : (&mut Pos, &mut Pos), number_of_iter: u8, pos_set: &mut HashSet<Pos>) {
    for _ in 0..number_of_iter {
        head.add_dir(&dir);
        make_tail_follow((*head, tail));
        pos_set.insert(tail.clone());
    }
}

fn make_tail_follow((head, tail) :  (Pos, &mut Pos)) {
    if tail.x.abs_diff(head.x) < 2 && tail.y.abs_diff(head.y) < 2 {
        // just do nothing
        // thiswas used to print debug, this branch is useless now
    } else {
        if tail.x < head.x {
            tail.x = tail.x + 1;
        } else if tail.x > head.x{
            tail.x = tail.x - 1;
        }

        if tail.y < head.y {
            tail.y = tail.y + 1;
        } else if tail.y > head.y{
            tail.y = tail.y - 1;
        }
    }
}

fn part2(input_vec: &Vec<(&str, u8)>) {
    let mut pos_set : HashSet<Pos> = HashSet::new();
    let pos_start  = Pos::new(0,0);
    pos_set.insert(pos_start);
    let mut knots : Vec<Pos> = vec!(Pos::new(0,0);10);

    for (dir, num) in input_vec {
        match dir {
            &"U" => {execute_n_moves_for_all((0,1), *num, &mut pos_set, &mut knots)},
            &"R" => {execute_n_moves_for_all((1,0), *num, &mut pos_set, &mut knots)},
            &"D" => {execute_n_moves_for_all((0,-1), *num, &mut pos_set, &mut knots)},
            &"L" => {execute_n_moves_for_all((-1,0), *num, &mut pos_set, &mut knots)},
            _ => {}
        }
    }
    println!("part2 res is {}", pos_set.len());
}
fn execute_n_moves_for_all(dir: (i16, i16), number_of_iter: u8, pos_set: &mut HashSet<Pos>, knots: &mut Vec<Pos>) {
    for _ in 0..number_of_iter {
        knots[0].add_dir(&dir);
       for i in 0..knots.len()-1 {
           let (head, tail) = (knots[i], &mut knots[i+1]);
           make_tail_follow((head, tail));
       }
        pos_set.insert(knots[knots.len()-1]);
    }
}


#[derive(Debug, Eq, Hash, PartialEq, Clone, Copy)]
struct Pos {
    x: i16,
    y: i16
}
impl Pos {
    fn new (x :i16, y: i16) -> Self{
        Self {
            x,
            y
        }
    }

    fn add_dir(&mut self, (x, y): &(i16, i16)) {
        self.x = self.x + x;
        self.y = self.y + y;
    }
}
