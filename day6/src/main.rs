use std::collections::HashSet;
use std::time::Instant;

fn main() {
    println!("Hello, world!");
    let input = include_str!("input.txt");

    let start = Instant::now();
    part1(input);
    println!("day1 took : {:?}", start.elapsed());

    let start = Instant::now();
    part2(input);
    println!("day2 took : {:?}", start.elapsed());
}

fn part1(input : &str) {
    let a =  input.as_bytes().windows(4).position(|s| {
        let mut set : HashSet<u8> = HashSet::new();
        for cha in s {
            set.insert(*cha);
        }
        set.len() == 4
    }).unwrap() + 4;
    println!("{}", a);
}

fn part2(input : &str) {
    let a =  input.as_bytes().windows(14).position(|s| {
        let mut set : HashSet<u8> = HashSet::new();
        for cha in s {
            set.insert(*cha);
        }
        set.len() == 14
    }).unwrap() + 14;
    println!("{}", a);
}