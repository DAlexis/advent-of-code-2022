use std::time::Instant;

fn main() {
    let input = include_str!("input_test.txt");
    let start = Instant::now();
    day1(input);
    println!("day1 took : {:?}", start.elapsed());

    let start2 = Instant::now();
    day2(input);
    println!("day2 took : {:?}", start2.elapsed());
}

fn day1(input: &str) {
    let sum = input
        .lines()
        .map(|line| line.split_once(",").unwrap())
        .map(|(first, second)| {
            let (first_min, first_max) = first.split_once("-").unwrap();
            let (second_min, second_max) = second.split_once("-").unwrap();
            let (first_min, first_max) = (first_min.parse::<i32>().unwrap(), first_max.parse::<i32>().unwrap());
            let (second_min, second_max) = (second_min.parse::<i32>().unwrap(), second_max.parse::<i32>().unwrap());
            if first_min <= second_min && first_max >= second_max {
                return 1
            }
            if second_min <= first_min && second_max >= first_max {
                return 1
            }
            0
        }).sum::<i32>();
    println!("{}",sum);
}

fn day2(input: &str) {
    let sum = input
        .lines()
        .map(|line| line.split_once(",").unwrap())
        .map(|(first, second)| {
            let (first_min, first_max) = first.split_once("-").unwrap();
            let (second_min, second_max) = second.split_once("-").unwrap();
            let (first_min, first_max) = (first_min.parse::<i32>().unwrap(), first_max.parse::<i32>().unwrap());
            let (second_min, second_max) = (second_min.parse::<i32>().unwrap(), second_max.parse::<i32>().unwrap());
            if first_min >= second_min && first_min <= second_max {
                return 1
            }
            if first_max >= second_min && first_max <= second_max {
                return 1
            }
            if second_min >= first_min && second_min <= first_max {
                return 1
            }
            if second_max >= first_min && second_max <= first_max {
                return 1
            }
            0
        }).sum::<i32>();
    println!("{}",sum);
}