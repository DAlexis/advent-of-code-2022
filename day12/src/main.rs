use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::env::current_exe;

fn main() {
    println!("Hello, world!");
    let input :&'static str = include_str!("./input.txt");
    part2(input);
}

fn part1(input: &'static str) {
    let (mut sx, mut sy, mut ex, mut ey) = (0, 0, 0, 0);
    let lines = input
        .split("\n")
        .collect::<Vec<&str>>();
    let mut all_nodes = lines
        .iter()
        .enumerate()
        .map(|(i, line)| {
            line.chars()
                .enumerate()
                .map(|(j, num)| {
                    if num == 'S' {
                        (sx, sy) = (i, j);
                        return (num.to_string().as_bytes()[0], 0);
                    } else if num == 'E' {
                        (ex, ey) = (i, j);
                        return ('z'.to_string().as_bytes()[0], 9999);
                    }
                    (num.to_string().as_bytes()[0], 99999)
                })
                .collect::<Vec<(u8, i32)>>()
        })
        .collect::<Vec<Vec<(u8, i32)>>>();
    let (height,width) = (all_nodes.len(), all_nodes[0].len());
    println!("{:?}", (ex, ey, sx, sy));

    let mut heap : BinaryHeap<ToProcess> = BinaryHeap::new();
    heap.push(ToProcess {i: sx, j: sy, poids: 0, height : all_nodes[sx][sy].0});

    parcours(&mut all_nodes, height, width, &mut heap);

    for i in 0..all_nodes.len() {
        for j in 0.. all_nodes[0].len() {
            print!("{}, ", all_nodes[i][j].1);
        }
        println!("");
    }
    println!("{:?}", all_nodes[ex][ey])
}


fn part2(input: &'static str) {
    let (mut sx, mut sy, mut ex, mut ey) = (0, 0, 0, 0);
    let lines = input
        .split("\n")
        .collect::<Vec<&str>>();
    let mut start_points: Vec<(usize, usize)> = vec![(0, 0)];
    let mut all_nodes = lines
        .iter()
        .enumerate()
        .map(|(i, line)| {
            line.chars()
                .enumerate()
                .map(|(j, num)| {
                    if num == 'S' {
                        (sx, sy) = (i, j);
                        start_points.push((i,j));
                        return (num.to_string().as_bytes()[0], 0);
                    } else if num == 'E' {
                        (ex, ey) = (i, j);
                        return ('z'.to_string().as_bytes()[0], 9999);
                    } else if num == 'a' {
                        start_points.push((i,j));
                    }
                    (num.to_string().as_bytes()[0], 99999)
                })
                .collect::<Vec<(u8, i32)>>()
        })
        .collect::<Vec<Vec<(u8, i32)>>>();
    let (height,width) = (all_nodes.len(), all_nodes[0].len());
    println!("{:?}", (ex, ey, sx, sy));

    let mut lowest = 9999;
    for start in start_points {
        let mut current = all_nodes.clone();
        current[start.0][start.1].1 = 0;
        let mut heap : BinaryHeap<ToProcess> = BinaryHeap::new();
        heap.push(ToProcess {i: start.0, j: start.1, poids: 0, height : current[start.0][start.1].0});
        parcours(&mut current, height, width, &mut heap);
        if current[ex][ey].1 < lowest {
            lowest = current[ex][ey].1;
        }
    }
    println!("{:?}", lowest);

}

fn parcours(all_nodes: &mut Vec<Vec<(u8, i32)>>, height: usize, width: usize, heap: &mut BinaryHeap<ToProcess>) {
    while !heap.is_empty() {
        let current = heap.pop().unwrap();
        //up
        if current.i > 0 {
            if all_nodes[current.i - 1][current.j].0 <= current.height + 1 && current.poids + 1 < all_nodes[current.i - 1][current.j].1 {
                all_nodes[current.i - 1][current.j].1 = current.poids + 1;
                heap.push(ToProcess { i: current.i - 1, j: current.j, poids: all_nodes[current.i - 1][current.j].1, height: all_nodes[current.i - 1][current.j].0 })
            }
        }
        //down
        if current.i < height - 1 {
            if (current.height == 'S'.to_string().as_bytes()[0] || all_nodes[current.i + 1][current.j].0 <= current.height + 1) && current.poids + 1 < all_nodes[current.i + 1][current.j].1 {
                all_nodes[current.i + 1][current.j].1 = current.poids + 1;
                heap.push(ToProcess { i: current.i + 1, j: current.j, poids: all_nodes[current.i + 1][current.j].1, height: all_nodes[current.i + 1][current.j].0 })
            }
        }
        //left
        if current.j > 0 {
            if all_nodes[current.i][current.j - 1].0 <= current.height + 1 && current.poids + 1 < all_nodes[current.i][current.j - 1].1 {
                all_nodes[current.i][current.j - 1].1 = current.poids + 1;
                heap.push(ToProcess { i: current.i, j: current.j - 1, poids: all_nodes[current.i][current.j - 1].1, height: all_nodes[current.i][current.j - 1].0 })
            }
        }
        //right
        if current.j < width - 1 {
            if (current.height == 'S'.to_string().as_bytes()[0] || all_nodes[current.i][current.j + 1].0 <= current.height + 1) && current.poids + 1 < all_nodes[current.i][current.j + 1].1 {
                all_nodes[current.i][current.j + 1].1 = current.poids + 1;
                heap.push(ToProcess { i: current.i, j: current.j + 1, poids: all_nodes[current.i][current.j + 1].1, height: all_nodes[current.i][current.j + 1].0 })
            }
        }
    }
}

#[derive( Debug)]
struct ToProcess {
    i: usize,
    j: usize,
    poids:i32,
    height: u8
}

impl Eq for ToProcess {}

impl PartialEq<Self> for ToProcess {
    fn eq(&self, other: &Self) -> bool {
        self.poids == other.poids
    }
}

impl PartialOrd<Self> for ToProcess {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for ToProcess {
    fn cmp(&self, other: &Self) -> Ordering {
        other.poids.cmp(&self.poids)
    }
}