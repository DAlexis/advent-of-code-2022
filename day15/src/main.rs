use std::collections::{HashMap, HashSet};

fn main() {
    println!("Hello, world!");
    let input = include_str!("input.txt");

     // part1(input);
    part2(input);
}

fn part1(input: &str) {
    let mut point_to_dist_map: HashMap<(i32, i32), i32> = HashMap::new();
    let mut beacons_set: HashSet<(i32, i32)> = HashSet::new();
    get_data(input, &mut point_to_dist_map, &mut beacons_set);

    println!("{:?}", point_to_dist_map);

    let row = 2000000;
    let minx = -6000000;
    let maxx = 6000000;
    println!("{:?} {}", minx, maxx);
    let mut total = 0;
    for x in minx..maxx {
        let mut to_add = false;
        for point_to_dist in &point_to_dist_map {
            if point_to_dist.1 >= &dist_manathan(*point_to_dist.0, (x, row)) {
                if !beacons_set.contains(&(x, row)) {
                    to_add = true;
                    break;
                }
            }
        }
        if to_add {
            total += 1;
            // println!("{:?}",(x, row));
        }
    }
    println!("{:?}", total);
}

fn part2(input: &str) {
    let mut point_to_dist_map: HashMap<(i32, i32), i32> = HashMap::new();
    let mut beacons_set: HashSet<(i32, i32)> = HashSet::new();
    get_data(input, &mut point_to_dist_map, &mut beacons_set);

    println!("{:?}", point_to_dist_map);

    let minx = 2500000;
    let maxx = 3000000;
    println!("{:?} {}", minx, maxx);
    let mut total = 0;
    let mut res = (0,0);
    for x in minx..maxx {
        for y in 3000000..4000000 {
            let mut to_add = true;
            for point_to_dist in &point_to_dist_map {
                if point_to_dist.1 >= &dist_manathan(*point_to_dist.0, (x, y)) {
                    to_add = false;
                    break;
                }
            }
            if to_add {
                total += 1;
                println!("{:?}", (x, y));
                res.0 = x;
                res.1 = y;
                break;
              //  println!("{:?}", (x, y));
            }
        }
        if res != (0,0) { break;}
        if x % 10000 ==0 {
            println!("{}", x);
        }
    }

    println!("{:?} {:?}", total, res);
    println!("{}", res.0 as i64 * 4000000 + res.1 as i64)
}

fn get_data(
    input: &str,
    mut point_to_dist_map: &mut HashMap<(i32, i32), i32>,
    mut beacons_set: &mut HashSet<(i32, i32)>,
) {

    input.lines().for_each(|line| {
        //sensor
        let (_, cut) = line.split_once("ensor at x=").unwrap();
        let (first, rest) = cut.split_once(":").unwrap();
        let sx = first.split_once(",").unwrap().0.parse::<i32>().unwrap();
        let sy = first.split_once("y=").unwrap().1.parse::<i32>().unwrap();

        //beacon
        let (_, second) = cut.split_once("beacon is at x=").unwrap();
        let bx = second.split_once(",").unwrap().0.parse::<i32>().unwrap();
        let by = second.split_once("y=").unwrap().1.parse::<i32>().unwrap();

        point_to_dist_map.insert((sx, sy), dist_manathan((sx, sy), (bx, by)));
        beacons_set.insert((bx, by));
    });
}

fn dist_manathan(first: (i32, i32), second: (i32, i32)) -> i32 {
    (first.0.abs_diff(second.0) + first.1.abs_diff(second.1)) as i32
}
