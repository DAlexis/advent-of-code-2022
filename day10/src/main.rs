use std::time::Instant;

fn main() {
    let input = include_str!("./input.txt");

    let start = Instant::now();
    part1(input);
    println!("day1 took : {:?}", start.elapsed());

    let start = Instant::now();
    part2(input);
    println!("day1 took : {:?}", start.elapsed());
}

fn part1(input: &str) {
    let lines = input.lines()
        .map(|line| {
            if line.starts_with("noop") {
                return ("noop", 0)
            }
            let (action, num) =line.split_once(" ").unwrap();
            let num = num.parse::<i32>().unwrap();
            (action, num)
        })
        .collect::<Vec<(&str, i32)>>();

    let (mut value, mut cycle, mut total) : (i32, i32, i32) = (1,1,0);
    for (action, line_value) in lines {
        match action {
            "addx" => {
                add_check_cycle(&mut cycle, &mut value, &mut total);
                value += line_value;
                add_check_cycle(&mut cycle, &mut value, &mut total);
            },
            "noop" => {
                add_check_cycle(&mut cycle, &mut value, &mut total);
            },
            _ => {}
        }
    }
    println!("part1 is {}", total);
}

fn add_check_cycle(cycle: &mut i32, value: &mut i32, total: &mut i32) {
    *cycle += 1;
    if *cycle % 40 == 20 {
        println!("cycle is {} and value : {}", cycle, value);
        *total += *cycle * *value;
    }
}


fn part2(input: &str) {
    let lines = input.lines()
        .map(|line| {
            if line.starts_with("noop") {
                return ("noop", 0)
            }
            let (action, num) =line.split_once(" ").unwrap();
            let num = num.parse::<i32>().unwrap();
            (action, num)
        })
        .collect::<Vec<(&str, i32)>>();

    let (mut value, mut cycle, mut total) : (i32, i32, i32) = (1,1,0);
    let mut all_lines_drawn = vec![String::new(); 6];
    let mut index : usize = 0;
    for (action, line_value) in lines {
        match action {
            "addx" => {
                add_draw_cycle(&mut cycle, &mut value, &mut total, &mut index, &mut all_lines_drawn, 0);

                add_draw_cycle(&mut cycle, &mut value, &mut total, &mut index, &mut all_lines_drawn, line_value);
            },
            "noop" => {
                add_draw_cycle(&mut cycle, &mut value, &mut total, &mut index, &mut all_lines_drawn, 0);
            },
            _ => {}
        }
    }
    all_lines_drawn.iter().for_each(|line| println!("{}", line));
}

fn add_draw_cycle(cycle: &mut i32, value: &mut i32, total: &mut i32, index: &mut usize, all_lines_drawn: &mut Vec<String>, line_value: i32) {
    if *cycle %40 >= *value && *cycle %40 <= *value + 2{
        all_lines_drawn[*index].push_str("#");
    } else {
        all_lines_drawn[*index].push_str(".");
    }
    *cycle += 1;
    *value += line_value;
    if *cycle % 40 == 1 {
        *index += 1;
    }

}