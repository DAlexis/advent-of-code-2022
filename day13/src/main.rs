use std::cmp::Ordering;

fn main() {
    println!("Hello, world!");
    let input: &str = include_str!("./input.txt");
    part2(input);
}

fn part1(input: &str) {
    let res = input.split("\n\n").enumerate().map(|(i, pair)| {

        let (first, second) = pair.split_once("\n").unwrap();
        let first = first.replace("10", "a");
        let second = second.replace("10", "a");
        return if compare_both(&first, &second) {
            i + 1
        } else {
            0
        }
    }).sum::<usize>();
    println!("{}", res);
}

fn part2(input: &str) {
    let  input = input.replace("\n\n", "\n");
    let mut input = input.replace("10", "a");
    input.push_str("\n[[2]]");
    input.push_str("\n[[6]]");
    let mut vec_input = input.lines().collect::<Vec<&str>>();
    vec_input.sort_by(|first, second| {
        if compare_both(first, second) {
            Ordering::Less
        } else {
            Ordering::Greater
        }
    });
    let res = vec_input.iter().enumerate().map(|(i, pair)| {

        if *pair == "[[2]]" || *pair == "[[6]]" {
            return i+1;
        } else {
            return 1;
        }
    }).fold(1, |acc, res| acc * res);
    vec_input.iter().for_each(|line| {
        println!("{}", line);
    });
    println!("{}", res);
}


fn compare_both(first :&str, second: &str) -> bool {
    println!("{} COMPARE TO {}", first, second);
    match (first.get(0..1), second.get(0..1)) {
        (Some("["), Some("[")) | (Some(","), Some(",")) | (Some("]"), Some("]"))=> {
            return compare_both(&first[1..], &second[1..])
        },
        (Some(","), Some("]")) => {
            return false
        },
        (Some("]"), Some(",")) => {
            return true
        },

        (Some("["), Some("]")) => {
            return false
        },
        (Some("]"), Some("["))  => {
            return true
        },
        (Some("["), _) => {
            let mut a = second.to_string();
            a.insert(1, ']');
            return compare_both(&first[1..],  &a);
        },
        (_, Some("[")) => {
            let mut a = first.to_string();
            a.insert(1, ']');
            return compare_both(&a, &second[1..]);
        },
        (Some("]"), _) => {
            return true
        },
        (_, Some("]")) => {
            return false
        },

        (_, _) => {
            let a = first.as_bytes()[0];
            let b = second.as_bytes()[0];
            if a < b {
                return true
            } else if b < a {
                return false
            } else {
                return compare_both(&first[1..], &second[1..]);
            }
        }
    }
}

fn is_list(current: &str) -> bool{
    return current.get(0..1) == Some("[") || current.get(0..1) == Some("]")
}
