use std::time::Instant;

fn main() {
    let input = include_str!("input.txt");

    let start = Instant::now();
    day1(input);
    println!("day1 took : {:?}",start.elapsed());

    let start = Instant::now();
    day2(input);
    println!("day2 took : {:?}",start.elapsed());
}

fn day1(input: &str) {
    let sum = input
        .split("\n\n")
        .map(|package | package
            .split('\n')
            .map(|calorie_umber| calorie_umber.parse::<i32>().unwrap())
            .sum::<i32>())
        .max().unwrap();

    println!("{:?}", sum);
}

fn day2(input: &str) {
    let mut vec = input
        .split("\n\n")
        .map(|package | package
            .split('\n')
            .map(|calorie_umber| calorie_umber.parse::<i32>().unwrap())
            .sum::<i32>())
        .collect::<Vec<i32>>();

    vec.sort_by(|a, b| b.cmp(a));
    vec.truncate(3);
    let sum : i32 = vec.iter().sum();
    println!("{:?}", sum);
}