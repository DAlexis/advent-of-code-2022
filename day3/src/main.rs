use std::collections::{HashMap, HashSet};
use std::time::Instant;

fn main() {

    let letter_to_value_map: HashMap<char, i32> = build_map();
    let input = include_str!("input.txt");

    let start = Instant::now();
    day1(input, &letter_to_value_map);
    println!("day1 took : {:?}", start.elapsed());

    let start = Instant::now();
    day2(input, &letter_to_value_map);
    println!("day2 took : {:?}", start.elapsed());
}


fn day1(input: &str, letter_to_value_map: &HashMap<char, i32>) {
    let sum = input.lines()
        .map(|line| {
            let (first, second) = line.split_at(line.len() /2);
            let first_set : HashSet<char> = HashSet::from_iter(first.chars());
            let second_set : HashSet<char> = HashSet::from_iter(second.chars());
            let intersect = first_set.intersection(&second_set);
            let line_result = intersect.map(|common_char| letter_to_value_map.get(common_char).unwrap())
                .sum::<i32>();
            line_result
        })
        .sum::<i32>();
    println!("{:?}", sum);
}
fn day2(input: &str, letter_to_value_map: &HashMap<char, i32>) {
    let mut lines = input.lines();
    let mut total = 0;
    while let Some(first) = lines.next() {
        let second = lines.next().unwrap();
        let third = lines.next().unwrap();

        let first_set : HashSet<char> = HashSet::from_iter(first.chars());
        let second_set : HashSet<char> = HashSet::from_iter(second.chars());
        let third_set : HashSet<char> = HashSet::from_iter(third.chars());

        let first_intersections : HashSet<&char> = HashSet::from_iter(first_set.intersection(&second_set));
        let second_intersections : HashSet<&char> =  HashSet::from_iter(first_set.intersection(&third_set));

        let group_of_three= first_intersections.intersection(&second_intersections).map(|common_char| letter_to_value_map.get(common_char).unwrap())
            .sum::<i32>();
        total = total + group_of_three;
    }
    println!("total day2 {}", total);
}



fn build_map() -> HashMap<char, i32> {
    let mut letter_to_value_map : HashMap<char, i32> = HashMap::with_capacity(52);
    let input = include_str!("value.txt");
    input.lines().for_each(|line| {
        let (letter, value) = line.split_once(" ").unwrap();
        letter_to_value_map.insert(letter.chars().nth(0).unwrap(), value.parse::<i32>().unwrap());
    });

    letter_to_value_map
}